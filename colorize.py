import scipy.ndimage as nd
import numpy as np
import cv2
from scipy.signal import convolve2d
import copy

def im2double(a):
    a = a.astype(np.float)
    a /= np.abs(a).max()
    return a

def colorize(img,imgStart,seg, method="stroke-preserving",color=[0,0,255]):
    if method=="stroke-preserving":
        imgbak = copy.deepcopy(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
        imgY = img[:,:,0]
        gaussFiltered = nd.gaussian_gradient_magnitude(imgStart, 5)
        grad = np.array(np.gradient(gaussFiltered))
        grad = np.sqrt(np.sum(np.square(grad)))
        haltIntensity = 1/(1 + grad)
        mod = np.abs(1 - haltIntensity)
        toConvolve = mod**2

        # print imgY.shape, toConvolve.shape

        # result = convolve2d(imgY,toConvolve)
        result = imgY * toConvolve
        # print "Convolution done"

        img[:,:,0] = result
        # img[:,:,1] = 0
        # img[:,:,2] = 0


        colorMask = cv2.cvtColor(img, cv2.COLOR_YUV2RGB)
        # colorMask = img

        for (x,y,z), val in np.ndenumerate(colorMask):
            if val > 0 and seg[x][y]:
                imgbak[x][y] =  color
        return imgbak

    elif method=="color-replacement":
        imgbak = copy.deepcopy(img)

        for (x,y,z), val in np.ndenumerate(imgbak):
            if val > 0 and seg[x][y]:
                imgbak[x][y] = color
        print imgbak.shape
        return imgbak





if __name__ == "__main__":
    disp = cv2.imread('./ichigo.jpg')
    imgStart = nd.imread('./ichigo.jpg', flatten=True)
    mask = np.zeros(imgStart.shape)
    mask[20:200, 20:200] = 1

    colorize()

    while True:
        cv2.imshow('image',img)
        key = cv2.waitKey(30) & 0xFF
        if key & 0xFF == 27:
            print 'Exiting'
            break;
