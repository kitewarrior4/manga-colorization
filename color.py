import cv2
import numpy as np
from Tkinter import Tk
from tkFileDialog import askopenfilename
import sys
import copy
from chanvese import chanvese
import scipy.ndimage as nd
from colorize import colorize


# if sys.argv[1]:
#     filename = sys.argv[1]
# else:
#     Tk().withdraw()
#     filename = askopenfilename()

# events = [i for i in dir(cv2) if 'EVENT' in i]
# print events

filename='ichigo.jpg'

img = cv2.imread(filename)
base = nd.imread(filename, flatten=True)
imgbk = copy.deepcopy(img)
imgBackground = copy.deepcopy(img)
palette = np.zeros([256,256,3], np.uint8)

r = 0
g = 0
b = 0
size = 25

Masks = []
baseMask = np.zeros(base.shape)
currMask = np.zeros(base.shape)

painting = False





def draw_circle(event,x,y,flags,param):
    global painting
    global Masks
    global currMask
    global baseMask
    global img

    if event == cv2.EVENT_LBUTTONDOWN:
        painting = True

    elif event == cv2.EVENT_LBUTTONUP:
        painting = False
        Masks.append((currMask,[b,g,r]))
        currMask = copy.deepcopy(baseMask)



    elif painting == True:
        cv2.circle(img, (x,y), size, (b,g,r), -1)
        cv2.circle(currMask, (x,y), size, 1, -1)



def nothing(x):
    pass


cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)

cv2.createTrackbar('R','image',0,255,nothing)
cv2.createTrackbar('G','image',0,255,nothing)
cv2.createTrackbar('B','image',0,255,nothing)
cv2.createTrackbar('size','image',0,50,nothing)



while(1):
    cv2.imshow('image', img)
    cv2.imshow('palette', palette)
    key = cv2.waitKey(30) & 0xFF
    if key & 0xFF == 27:
        print 'Exiting'
        break;

    if key == ord('r'):
        img = copy.deepcopy(imgbk)

    r = cv2.getTrackbarPos('R','image')
    g = cv2.getTrackbarPos('G','image')
    b = cv2.getTrackbarPos('B','image')

    size = cv2.getTrackbarPos('size','image')

    palette[:] = [b,g,r]

    if key == ord('d'):
        for i,obj in enumerate(Masks):
            mask = obj[0]
            color = obj[1]
            seg,phi,it = chanvese(base, mask, max_its=500, display=True, alpha=1.0)
            img = colorize(imgBackground,base,seg,color=color,method="color-replacement")
            # for (x,y,z),val in np.ndenumerate(img):
            #     if seg[x][y]:
            #         img[x][y] = color
            imgBackground = copy.deepcopy(img)


cv2.imwrite('result.jpg',img)
cv2.destroyAllWindows()
